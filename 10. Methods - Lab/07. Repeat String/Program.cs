﻿string text = Console.ReadLine();

int repeater = int.Parse(Console.ReadLine());

NewFunction(repeater, text, out var result1, out var i1);

string NewFunction(int repeater1, string? s, out string result, out int i)
{
    result = string.Empty;
    for (i = 0; i < repeater1; i++)
    {
        Console.Write(result + s);
    }
    return result;
}

//OR
//string text = Console.ReadLine();
//int n = int.Parse(Console.ReadLine());

//string result = RepeatString(text, n);

//Console.WriteLine(result);

//static string RepeatString(string text, int n)
//{
//    string result = string.Empty;
//    for (int i = 0; i < n; i++)
//    {
//        result += text;
//    }
//    return result;
//}