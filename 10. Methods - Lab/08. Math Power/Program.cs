﻿double baseNumber = double.Parse(Console.ReadLine()); 
double powerNumber = double.Parse(Console.ReadLine());

MathPowFunction(baseNumber, powerNumber, out var result1);

double MathPowFunction(double baseNum, double powerNum, out double result)
{
    result = Math.Pow(baseNum, powerNum);
    Console.WriteLine(result);
    return result;
}