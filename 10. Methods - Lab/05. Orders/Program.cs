﻿string product = Console.ReadLine();
int quantity = int.Parse(Console.ReadLine());

NewFunction(product, quantity);

void NewFunction(string? s, int i)
{
    switch (s)
    {
        case "coffee":
            Console.WriteLine($"{(1.5 * i):f2}");
            break;
        case "water":
            Console.WriteLine($"{(1.0 * i):f2}");
            break;
        case "coke":
            Console.WriteLine($"{(1.4 * i):f2}");
            break;
        case "snacks":
            Console.WriteLine($"{(2.0 * i):f2}");
            break;
    }
}