﻿string command = Console.ReadLine();

DiffrentInputs(command);

void DiffrentInputs(string? s)
{
    if (s == "int")
    {
        int input = int.Parse(Console.ReadLine());
        Console.WriteLine(input * 2);
    }
    else if (s == "real")
    {
        double input = double.Parse(Console.ReadLine());
        double result = input * 1.5;
        Console.WriteLine($"{result:f2}");
    }
    else
    {
        string input = Console.ReadLine();
        Console.WriteLine($"${input}$");
    }
}