﻿int[] array = Console.ReadLine()
    .Split(" ", StringSplitOptions.RemoveEmptyEntries)
    .Select(int.Parse)
    .ToArray();
int n = int.Parse(Console.ReadLine());

for (int i = 0; i < array.Length; i++)
{
    for (int j = i + 1; j < array.Length; j++)
    {
        if (array[i] + array[j] == n)
        {
            Console.WriteLine($"{array[i]} {array[j]}");
        }
    }
}


//OR

//int[] numbers = Console.ReadLine()
//    .Split()
//    .Select(int.Parse)
//    .ToArray();
//int sum = int.Parse(Console.ReadLine());

//for (int i = 0; i < numbers.Length; i++)
//{
//    for (int j = i + 1; j < numbers.Length; j++)
//    {
//        if (sum == numbers[i] + numbers[j])
//        {
//            Console.WriteLine("{0} {1}", numbers[i], numbers[j]);
//        }
//    }
//}