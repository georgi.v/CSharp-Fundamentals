﻿int[] arr = Console.ReadLine()
    .Split(" ", StringSplitOptions.RemoveEmptyEntries)
    .Select(int.Parse)
    .ToArray();
long rotationCount = long.Parse(Console.ReadLine());

long timesToRotate = rotationCount % arr.Length;
for (int r = 1; r <= timesToRotate; r++)
{
    int firstEl = arr[0];
    for (int i = 1; i < arr.Length; i++)
    {
        arr[i - 1] = arr[i];
    }
    arr[arr.Length - 1] = firstEl;
}
Console.WriteLine(string.Join(" ", arr));

//OR

//int[] inputNumbers = Console.ReadLine()
//    .Split()
//    .Select(int.Parse)
//    .ToArray();

//int rotation = int.Parse(Console.ReadLine());

//for (int j = 0; j < rotation; j++)
//{
//    int firstElement = inputNumbers[0];

//    for (int i = 0; i < inputNumbers.Length - 1; i++)
//    {
//        inputNumbers[i] = inputNumbers[i + 1];
//    }
//    inputNumbers[inputNumbers.Length - 1] = firstElement;
//}

//Console.WriteLine(string.Join(" ", inputNumbers));

//OR

//int[] array = Console.ReadLine()
//    .Split(" ", StringSplitOptions.RemoveEmptyEntries)
//    .Select(int.Parse)
//    .ToArray();
//int n = int.Parse(Console.ReadLine());
//int firstElement = 0;
//for (int i = 0; i < n; i++)
//{
//    firstElement = array[0];
//    int[] temp = new int[array.Length];
//    for (int j = 1; j < array.Length; j++)
//    {
//        temp[j - 1] = array[j];
//    }
//    temp[array.Length - 1] = firstElement;
//    array = temp;
//}
//Console.WriteLine(string.Join(" ", array));