﻿int n = int.Parse(Console.ReadLine());

int[] wagons = new int[n];

int peopleCount = 0;

for (int i = 0; i < wagons.Length; i++)
{
    int people = int.Parse(Console.ReadLine());
    wagons[i] = people;
    peopleCount += people;
}
Console.WriteLine(string.Join(" ", wagons));
Console.WriteLine(peopleCount);

//OR

//int n = int.Parse(Console.ReadLine());
//int[] wagons = new int[n];
//int sum = 0;

//for (int i = 0; i < n; i++)
//{
//    int currentNumber = int.Parse(Console.ReadLine());
//    wagons[i] = currentNumber;
//    sum += wagons[i];
//}
//foreach (var wagon in wagons)
//{
//    Console.Write(wagon + " ");
//}
//Console.WriteLine();
//Console.WriteLine(sum);