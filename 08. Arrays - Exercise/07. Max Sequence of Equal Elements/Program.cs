﻿        int[] array = Console.ReadLine().Split().Select(int.Parse).ToArray();
        int[] result = null;
        int currentCount = 1, longestCount = 0;

        for (int i = 0; i < array.Length - 1; i++)
        {
            if (array[i] == array[i + 1])
            {
                currentCount++;
                if (currentCount > longestCount)
                {
                    longestCount = currentCount;
                    result = new int[longestCount];
                    for (int j = 0; j < longestCount; j++)
                    {
                        result[j] = array[i];
                    }
                }
            }
            else
            {
                currentCount = 1;
            }
        }
        Console.WriteLine(string.Join(" ", result));


//OR

//        int[] numbers = Console.ReadLine()
//            .Split()
//            .Select(int.Parse)
//            .ToArray();

//        int longestSequenceStartIndex = 0;
//        int longestSequenceLength = 0;

//        for (int i = 1; i < numbers.Length; i++)
//        {
//            int currentSequenceStartIndex = i - 1;
//            int currentSequenceLength = 1;

//            while (i < numbers.Length && numbers[i] == numbers[i - 1])
//            {
//                currentSequenceLength++;
//                i++;
//            }

//            if (currentSequenceLength > longestSequenceLength)
//            {
//                longestSequenceLength = currentSequenceLength;
//                longestSequenceStartIndex = currentSequenceStartIndex;
//            }
//        }

//        for (int i = longestSequenceStartIndex; i < longestSequenceStartIndex + longestSequenceLength; i++)
//        {
//            Console.Write($"{numbers[i]} ");
//        }