﻿int n = int.Parse(Console.ReadLine());

int[] firstArray = new int[n];
int[] secondArray = new int[n];
for (int row = 1; row <= n; row++)
{
    int[] numbers = Console.ReadLine()
        .Split(" ")
        .Select(int.Parse)
        .ToArray();
    int firstNum = numbers[0];
    int secondNum = numbers[1];

    if (row % 2 != 0)
    {
        firstArray[row - 1] = firstNum;
        secondArray[row - 1] = secondNum;
    }
    else
    {
        firstArray[row - 1] = secondNum;
        secondArray[row - 1] = firstNum;
    }
}
Console.WriteLine(string.Join(" ", firstArray));
Console.WriteLine(string.Join(" ", secondArray));