﻿List<int> numbers = new List<int>();

numbers.Add(1);
numbers.AddRange(new int[] { 2, 3, 4 });
numbers.Insert(0, 0);
numbers.InsertRange(numbers.Count - 1, new int[] { 5, 6, 7 });
numbers.RemoveAt(0);
numbers.RemoveRange(0, 2);