﻿using System.Xml.Linq;

List<string> names = new List<string>();

names.Add("Maria");
names.Add("Ivan");
names.Add("Asen");
names.Add("Peter");

Console.WriteLine(string.Join(", ", names));

names.Insert(1, "George");

names.Remove("Maria");

Console.WriteLine(string.Join(", ", names));
Console.WriteLine($"Count: {names.Count}");

bool isIvanInList = names.Contains("Ivan");

if (names.Contains("Peter"))
{
    Console.WriteLine("Peter is in the list");
}
else
{
    Console.WriteLine("No");
}

names.Sort();

Console.WriteLine(string.Join(", ", names));

List<int> numbers = new List<int>
{
    1, 2, 3, 4, 5
};
numbers.Reverse();
Console.WriteLine(string.Join(", ", numbers));


string values = Console.ReadLine();
List<string> items = values.Split(' ').ToList();

List<int> nums = new List<int>();

for (int i = 0; i < items.Count; i++)
{
    nums.Add(int.Parse(items[i]));
}