﻿//Constant options for validation
const int passwordMinLength = 6;
const int passwordMaxLength = 10;
const int passwordDigitsMinCount = 2;

//Read password from the Console
string password = Console.ReadLine();

//TODO: Validate given password
bool isPasswordValid = ValidatePassword(password, passwordMinLength, passwordMaxLength, passwordDigitsMinCount);

//Print if it is valid
if (isPasswordValid)
{
    Console.WriteLine("Password is valid");
}

/// <summary>
/// The given method validates password. It prints any validation errors and return boolean whether the password is valid
/// </summary>
static bool ValidatePassword(string password, int passwordMinLength, int passwordMaxLength, int passwordDigitsMinCount)
{
    bool isPasswordValid = true;
    if (!ValidatePasswordLength(password, passwordMinLength, passwordMaxLength))
    {
        Console.WriteLine($"Password must be between {passwordMinLength} and {passwordMaxLength} characters");
        isPasswordValid = false;
    }
    if (!ValidatePasswordIsAlphaNumerical(password))
    {
        Console.WriteLine("Password must consist only of letters and digits");
        isPasswordValid = false;
    }
    if (!ValidatePasswordDigitsMinCount(password, passwordDigitsMinCount))
    {
        Console.WriteLine($"Password must have at least {passwordDigitsMinCount} digits");
        isPasswordValid = false;
    }

    return isPasswordValid;
}

static bool ValidatePasswordLength(string password, int minLength, int maxLength)
{
    if (password.Length >= minLength && password.Length <= maxLength)
    {
        return true;
    }

    return false;
}

static bool ValidatePasswordIsAlphaNumerical(string password)
{
    foreach (char ch in password)
    {
        if (!Char.IsLetterOrDigit(ch))
        {
            return false;
        }
    }

    return true;
}

static bool ValidatePasswordDigitsMinCount(string password, int minDigitsCount)
{
    int digitsCount = 0;
    foreach (char ch in password)
    {
        if (Char.IsDigit(ch))
        {
            digitsCount++;
        }
    }

    return digitsCount >= minDigitsCount;
}

//OR

//string password = Console.ReadLine();
//bool isValid = true;

//if (!IsLegithValid(password))
//{
//    isValid = false;
//    Console.WriteLine("Password must be between 6 and 10 characters");
//}

//if (!OnlyContainsDigitsAndLetter(password))
//{
//    isValid = false;
//    Console.WriteLine("Password must consist only of letters and digits");
//}

//if (!ContainsAtLeast2Digits(password))
//{
//    isValid = false;
//    Console.WriteLine("Password must have at least 2 digits");
//}

//if (isValid)
//{
//    Console.WriteLine("Password is valid");
//}

//bool IsLegithValid(string password)
//{
//    return password.Length >= 6 && password.Length <= 10;
//}

//bool OnlyContainsDigitsAndLetter(string password)
//{
//    return password.All(char.IsLetterOrDigit);
//}

//bool ContainsAtLeast2Digits(string password)
//{
//    return password.Count(char.IsDigit) >= 2;
//}
