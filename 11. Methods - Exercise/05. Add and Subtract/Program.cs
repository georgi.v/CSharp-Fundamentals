﻿int firstInt = int.Parse(Console.ReadLine());
int secondInt = int.Parse(Console.ReadLine());
int thirdInt = int.Parse(Console.ReadLine());

int result = Sum(firstInt, secondInt);
Subtract(thirdInt, result);

static int Sum(int firstInt, int secondInt)
{
    int sum = firstInt + secondInt;

    return sum;
}

void Subtract(int thirdInt, int result)
{
    int subtract = result - thirdInt;

    Console.WriteLine(subtract);
}