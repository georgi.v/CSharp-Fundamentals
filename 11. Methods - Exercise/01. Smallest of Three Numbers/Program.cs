﻿int first = int.Parse(Console.ReadLine());
int second = int.Parse(Console.ReadLine());
int third = int.Parse(Console.ReadLine());

//Console.WriteLine(Math.Min(first, Math.Min(second, third)));

PrintSmallestNumber(first, second, third);

void PrintSmallestNumber(int first, int second, int third)
{
    if (first <= second && first <= third)
    {
        Console.WriteLine(first);
    }
    else if (second <= first && second <= third)
    {
        Console.WriteLine(second);
    }
    else if (third <= first && third <= second)
    {
        Console.WriteLine(third);
    }
}


//OR

//int firstNum = int.Parse(Console.ReadLine());
//int secondNum = int.Parse(Console.ReadLine());
//int thirdNum = int.Parse(Console.ReadLine());


//int result = SmallestNumber(firstNum, secondNum, thirdNum);
//Console.WriteLine(result);

//int SmallestNumber(int n1, int n2, int n3)
//{
//    if (n1 < n2 && n1 < n3)
//    {
//        return n1;
//    }
//    else if (n2 < n3 && n2 < n1)
//    {
//        return n2;
//    }
//    else
//    {
//        return n3;
//    }
//}