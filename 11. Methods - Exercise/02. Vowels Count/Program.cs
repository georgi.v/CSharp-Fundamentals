﻿//Read single string from Console
string word = Console.ReadLine();

//TODO: Creates method which returns vowels count
int vowelsCount = GetVowelsCount(word);

//Print vowels count
Console.WriteLine(vowelsCount);


static int GetVowelsCount(string word)
{
    //Creates array with all vowels characters
    char[] vowels = new char[] { 'a', 'e', 'i', 'o', 'u' };

    int vowelsCount = 0;
    //for (int i = 0; i < word.Length; i++)
    //{
    //    char ch = Char.ToLower(word[i]);
    //    if (vowels.Contains(ch))
    //    {
    //        //Current character is a vowel
    //        vowelsCount++;
    //    }
    //}
    foreach (char ch in word.ToLower())
    {
        if (vowels.Contains(ch))
        {
            //Current character is a vowel
            vowelsCount++;
        }
    }

    return vowelsCount;
}

//OR

//string input = Console.ReadLine();

//Console.WriteLine(VowelsCount(input));
//int VowelsCount(string input)
//{
//    int count = 0;
//    foreach (var character in input)
//    {
//        if(isVowel(character))
//        {
//            count++;
//        }
//    }
//    return count;
//}

//bool isVowel(char character)
//{
//    //char[] vowels = new[] { 'a', 'o', 'e', 'u' };
//    //return vowels.Contains(character);
//    return "aeiouyAEIOUY".IndexOf(character) >= 0;
//}


//OR

//string input = Console.ReadLine().ToLower();

////if (input.ToLower().Contains('e', 'i', 'o', 'u', 'a'))
////{

////}
//var counter = VowelsCount(input, out var i1);
//Console.WriteLine(counter);
//int VowelsCount(string s, out int i)
//{
//    int counter1 = 0;
//    for (i = 0; i < s.Length; i++)
//    {
//        if (s[i] == 'e' || s[i] == 'i' || s[i] == 'o' || s[i] == 'u' || s[i] == 'a')
//        {
//            counter1++;
//        }
//    }

//    return counter1;
//}
