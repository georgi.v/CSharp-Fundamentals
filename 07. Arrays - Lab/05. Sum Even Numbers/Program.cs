﻿int[] numbers = Console.ReadLine().Split().Select(int.Parse).ToArray();
int sum = 0;

foreach (var number in numbers)
{
    if (number % 2 == 0)
    {
        sum += number;
    }
}

Console.WriteLine(sum);

//OR
//int[] input = Console.ReadLine()
//    .Split()
//    .Select(int.Parse)
//    .ToArray();

//int evenSum = 0;

//for (int i = 0; i < input.Length; i++)
//{
//    if (input[i] %2 == 0)
//    {
//        evenSum += input[i];
//    }
//}
//Console.WriteLine(evenSum);