﻿int input = int.Parse(Console.ReadLine());

string[] days = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

if (input < 1 || input > 7)
{
    Console.WriteLine("Invalid day!");
}
else
{
    Console.WriteLine(days[input - 1]);
}

//Or
//int dayOfWeek = int.Parse(Console.ReadLine());
//string[] weekDays =
//{
//    "Monday",
//    "Tuesday",
//    "Wednesday",
//    "Thursday",
//    "Friday",
//    "Saturday",
//    "Sunday"
//};

//if (dayOfWeek >= 1 && dayOfWeek <= weekDays.Length)
//{
//    Console.WriteLine(weekDays[dayOfWeek - 1]);
//}
//else
//{
//    Console.WriteLine("Invalid day!");
//}