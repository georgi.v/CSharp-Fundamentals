﻿int[] inputNumbers = Console.ReadLine()
    .Split()
    .Select(int.Parse)
    .ToArray();
for (int i = 0; i < inputNumbers.Length - 1; i++)
{
    for (int j = 0; j < inputNumbers.Length - 1 - i; j++)
    {
        inputNumbers[j] = inputNumbers[j] + inputNumbers[j + 1];
    }
}
Console.WriteLine(inputNumbers[0]);

//OR

//int[] numbers = Console.ReadLine().Split().Select(int.Parse).ToArray();
//int[] condensed = new int[numbers.Length - 1];

//while (numbers.Length > 1)
//{
//    for (int i = 0; i < numbers.Length - 1; i++)
//    {
//        condensed[i] = numbers[i] + numbers[i + 1];

//        if (i == numbers.Length - 2)
//        {
//            numbers = condensed;
//            condensed = new int[numbers.Length - 1];
//        }
//    }
//}
//Console.WriteLine(numbers[0]);