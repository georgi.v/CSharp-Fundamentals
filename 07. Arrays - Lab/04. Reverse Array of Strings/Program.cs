﻿string[] input = Console.ReadLine()
    .Split()
    .ToArray();
Array.Reverse(input);

for (int i = 0; i < input.Length; i++)
{
    Console.Write(input[i] + " ");
}

//OR

//Console.WriteLine(string.Join(" ", Console.ReadLine().Split(" ", StringSplitOptions.RemoveEmptyEntries).Reverse()));