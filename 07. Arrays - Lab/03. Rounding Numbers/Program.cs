﻿double[] input = Console.ReadLine()
    .Split()
    .Select(double.Parse)
    .ToArray();
for (int i = 0; i < input.Length; i++)
{
    Console.WriteLine($"{input[i]} => {(int)Math.Round(input[i], MidpointRounding.AwayFromZero)}");
}

//OR
//string[] input = Console.ReadLine().Split(' ');
//double[] numbers = new double[input.Length];

//for (int i = 0; i < numbers.Length; i++)
//{
//    numbers[i] = double.Parse(input[i]);
//}

//for (int i = 0; i < numbers.Length; i++)
//{
//    Console.WriteLine($"{numbers[i]} => {(int)Math.Round(numbers[i], MidpointRounding.AwayFromZero)}");
//}