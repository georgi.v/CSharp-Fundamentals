﻿int[] firstArray = Console.ReadLine()
    .Split(" ", StringSplitOptions.RemoveEmptyEntries)
    .Select(int.Parse)
    .ToArray();
int[] secondArray = Console.ReadLine()
    .Split(" ", StringSplitOptions.RemoveEmptyEntries)
    .Select(int.Parse)
    .ToArray();
bool isDiffrent = false;
int sum = 0;

for (int i = 0; i < firstArray.Length; i++)
{
    if (firstArray[i] != secondArray[i])
    {
        isDiffrent = true;
        Console.WriteLine($"Arrays are not identical. Found difference at {i} index");
        break;
    }
    sum += firstArray[i];

    if (isDiffrent)
    {
        break;
    }
}

if (!isDiffrent)
{
    Console.WriteLine($"Arrays are identical. Sum: {sum}");
}

//OR

//int[] firstArray = Console.ReadLine().Split().Select(int.Parse).ToArray();
//int[] secondArray = Console.ReadLine().Split().Select(int.Parse).ToArray();

//int sum = 0;

//for (int index = 0; index < firstArray.Length; index++)
//{
//    if (firstArray[index] != secondArray[index])
//    {
//        Console.WriteLine($"Arrays are not identical. Found difference at {index} index");
//        return;
//    }
//    else
//    {
//        sum += firstArray[index];
//    }
//}
//Console.WriteLine($"Arrays are identical. Sum: {sum}");


//OR
//int[] arr1 = Console.ReadLine()
//    .Split()
//    .Select(int.Parse)
//    .ToArray();

//int[] arr2 = Console.ReadLine()
//    .Split()
//    .Select(int.Parse)
//    .ToArray();
//int arrSum = 0;

//for (int i = 0; i < arr1.Length; i++)
//{
//    for (int j = 0; j < arr1.Length; j++)
//    {

//    }
//    for (int k = 0; k < arr2.Length; k++)
//    {

//    }
//    if (arr1[i] != arr2[i])
//    {
//        Console.WriteLine($"Arrays are not identical. Found difference at {i} index");
//        return;
//    }
//    arrSum += arr1[i]; arrSum += arr2[i];
//}
//Console.WriteLine($"Arrays are identical. Sum: {arrSum /2}");