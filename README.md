# SoftUni-CSharpFundamentals 2023
🎓 Repository for students of the course Programming Fundamentals with C# 2023 @ SoftUni. Here I will upload all demos and solutions during exercises.
***
# Exercise List
*Here you can find problem solutions for the following exercises:*
1. [**Basic Syntax, Conditional Statements and Loops**]()
2. [**Data Types and Variables**]()
3. [**Arrays**]()
4. [**Methods**]()
5. [**List**]()
6. [**Objects and Classes**]()
7. [**Associative Arrays**]()
8. [**Text Processing**]()
9. [**Regular Expressions**]()
10. [**Exam Preparation**]()

👊
